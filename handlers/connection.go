package handlers

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-protocol"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/hathi"
	"log"
	"net"
	"time"
)

func HandleConnection(engine *hathi.Core, connection net.Conn) {
	// Turn connection into a hathi endpoint
	endpoint := protocol.NewEndpointKernel(connection)
	endpoint.SetImplementedFunctions([]string{"get-object", "post-outbox",
		"new-actor", "delete-actor"})
	endpoint.Run()
	defer endpoint.Stop()
	// Loop till close
	var err error
	for true {
		// Recieve packet
		if endpoint.IsClosed() {
			break
		}
		packet := endpoint.GetPacket()
		if packet == nil {
			time.Sleep(10 * time.Millisecond)
			continue
		}
		// Validate packet, what do we need to check?
		// Call relevant handler, get result
		result := new(protocol.Packet)
		if Logging {
			log.Println("Got packet", packet.Function)
		}
		// TODO: post-inbox
		switch packet.Function {
		case "get-object":
			var object *activitypub.Object
			object, result.Error, err = engine.RetrieveObject(packet.ObjectID,
				packet.Actor)
			result.Object = object.Mapify()
		case "post-outbox":
			object, _ := activitypub.Objectify(packet.Object)
			// TODO: test object validity
			result.ObjectID, result.Error, err = engine.PostToOutbox(
				packet.Actor, object)
		case "new-actor":
			result.Error, result.Actor, err = engine.CreateActor(packet.Actor)
		case "delete-actor":
			result.Error, err = engine.DeleteActor(packet.Actor)
		}
		if err != nil {
			if Logging {
				log.Println("ERROR:", packet.Function, err)
			}
		}
		// Fill in universal details of result packet
		result.Function = "result"
		result.Version = 1
		result.RequestID = packet.RequestID
		// Send result packet
		endpoint.SendPacket(result)
	}
}
