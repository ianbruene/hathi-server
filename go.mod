module gitlab.com/hathi-social/hathi-server

require (
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/peterhellberg/duration v0.0.0-20190124114432-484232d632c1
	gitlab.com/hathi-social/hathi-protocol v0.0.0-20190410143151-ff15e491ea8c
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
