package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	//"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"sort"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
)

func (h *HathiDB) SetObjectPrivileges(tx *sql.Tx, objID string, actors map[string][]string) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Sort our actors. If they aren't sorted we can't reliably test
	sortedActors := make([]string, len(actors))
	i := 0
	for actorID, _ := range actors {
		sortedActors[i] = string(actorID)
		i++
	}
	sort.Strings(sortedActors)
	// Clear old owners for this object
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1;", objID)
	if err != nil {
		return err
	}
	// Set new data
	query := "insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);"
	for _, actorID := range sortedActors {
		for _, priv := range actors[actorID] {
			_, err = tx.Exec(query, objID, actorID, priv)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (h *HathiDB) GetObjectPrivileges(tx *sql.Tx, objID string) (owners map[string][]string, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	owners = make(map[string][]string)
	ownerTable, err := h.DB.Query(`select ACTOR, BITNAME from PRIVILEGE where ID = $1`, objID)
	if err != nil {
		return nil, err
	}
	defer ownerTable.Close()
	var actor string
	var bitname string
	for ownerTable.Next() {
		err = ownerTable.Scan(&actor, &bitname)
		if err != nil {
			return nil, err
		}
		if _, ok := owners[actor]; ok == false {
			owners[actor] = make([]string, 0)
		}
		owners[actor] = append(owners[actor], bitname)
	}
	return owners, nil
}

func (h *HathiDB) SetObjActPrivileges(tx *sql.Tx, objID string, actorID string, privs []string) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Clear old data
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1 and ACTOR = $2",
		objID, actorID)
	if err != nil {
		return err
	}
	// Set new data
	query := "insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);"
	for _, priv := range privs {
		_, err = tx.Exec(query, objID, actorID, priv)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) GetObjActPrivileges(tx *sql.Tx, objID string, actorID string) (privs []string, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	privs = make([]string, 0)
	actorTable, err := h.DB.Query(`select BITNAME from PRIVILEGE where ID = $1 and ACTOR = $2;`, objID, actorID)
	if err != nil {
		return nil, err
	}
	defer actorTable.Close()
	var bitname string
	for actorTable.Next() {
		err = actorTable.Scan(&bitname)
		if err != nil {
			return nil, err
		}
		privs = append(privs, bitname)
	}
	return privs, nil
}

func (h *HathiDB) CheckPrivilege(tx *sql.Tx, objID string, actorID string, bitname string) (set bool, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return false, err
		}
		defer h.EndTransaction(tx, &err)
	}
	dataTable, err := h.DB.Query(`select * from PRIVILEGE where ID = $1 and ACTOR = $2 and BITNAME = $3;`, objID, actorID, bitname)
	if err != nil {
		return false, err
	}
	defer dataTable.Close()
	if dataTable.Next() == true {
		return true, nil
	} else {
		if err = dataTable.Err(); err != nil {
			return false, err
		} else {
			return false, nil
		}
	}
}

func (h *HathiDB) SetPrivilege(tx *sql.Tx, objID string, actorID string, bitname string, value bool) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	_, err = tx.Exec("delete from PRIVILEGE where ID = $1 and ACTOR = $2 and BITNAME = $3;", objID, actorID, bitname)
	if err != nil {
		return err
	}
	if value == true {
		_, err = tx.Exec("insert into PRIVILEGE (ID, ACTOR, BITNAME) values ($1, $2, $3);", objID, actorID, bitname)
		if err != nil {
			return err
		}
	}
	return nil
}
