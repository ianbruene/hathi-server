package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"reflect"
	"testing"

	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseLists(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// ======================================================
	// Test getI
	// ======================================================
	// Exists
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	value, err := db.ListGetI(nil, "foo", "field", 1)
	if err != nil {
		t.Fatal("Database CollectionGetI failed:", err)
	}
	if value != "baz" {
		t.Fatal("Database CollectionGetI equality failed:", value)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionGetI: unfulfilled expectations:", err)
	}

	// Doesn't exist
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"})
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	value, err = db.ListGetI(nil, "foo", "field", 1)
	if err != nil {
		t.Fatal("Database CollectionGetI non-existing failed:", err)
	}
	if value != nil {
		t.Fatal("Database CollectionGetI non-existing equality failed:", value)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionGetI non-existing: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test getRange
	// ======================================================
	// Bounded upwards
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "").
		AddRow(STRING, "quux", 0, 0.0, false, "")
	mock.ExpectBegin()
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 1, 3).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err := db.ListGetRange(nil, "foo", "field", 1, 2)
	if err != nil {
		t.Fatal("Database CollectionGetRange upwards failed:", err)
	}
	items := []interface{}{"baz", "quux"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange upwards equality failed:", entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange upwards: unfulfilled expectations:",
			err)
	}

	// Bounded downwards
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "").
		AddRow(STRING, "quux", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", -1, 2).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err = db.ListGetRange(nil, "foo", "field", 1, -2)
	if err != nil {
		t.Fatal("Database CollectionGetRange downwards failed:", err)
	}
	items = []interface{}{"baz", "quux"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange downwards equality failed:",
			entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange downwards: unfulfilled expectations:",
			err)
	}

	// Unbounded
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "").
		AddRow(STRING, "S.I.", 0, 0.0, false, "").
		AddRow(STRING, "quux", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err = db.ListGetRange(nil, "foo", "field", 1, 0)
	if err != nil {
		t.Fatal("Database CollectionGetRange unbounded failed:", err)
	}
	items = []interface{}{"baz", "S.I.", "quux"}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange unbounded equality failed:",
			entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange unbounded: unfulfilled expectations:",
			err)
	}

	// Doesn't exist
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"})
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 1).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	entries, err = db.ListGetRange(nil, "foo", "field", 1, 0)
	if err != nil {
		t.Fatal("Database CollectionGetRange non-existing failed:", err)
	}
	items = []interface{}{}
	if reflect.DeepEqual(entries, items) != true {
		t.Fatal("Database CollectionGetRange non-existing equality failed:",
			entries)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database CollectionGetRange non-existing: unfulfilled expectations:",
			err)
	}

	// ======================================================
	// Test append
	// ======================================================
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(false)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"MAXID"}).
		AddRow(1)
	mock.ExpectQuery("select max\\(ITEMINDEX\\) as MAXID from FIELDDATA").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDDATA").
		WithArgs("foo", "field", 2, STRING, "item", 0, 0.0, false, "").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ListAppend(nil, "foo", "field", "item")
	if err != nil {
		t.Fatal("Database CollectionAppend failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database CollectionAppend: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test addI
	// ======================================================
	// Have to move an item
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(false)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "blah", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(5).
		AddRow(4)
	mock.ExpectQuery(
		"select ITEMINDEX from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDDATA").
		WithArgs(6, "foo", "field", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update FIELDDATA").
		WithArgs(5, "foo", "field", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("insert into FIELDDATA").
		WithArgs("foo", "field", 4, STRING, "neo", 0, 0.0, false, "").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ListAddI(nil, "foo", "field", 4, "neo")
	if err != nil {
		t.Fatal("Database ListAddI moving failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database ListAddI moving: unfulfilled expectations:", err)
	}

	// Don't have to move an item
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(false)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "blah", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"})
	mock.ExpectQuery(
		"select ITEMINDEX from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDDATA").
		WithArgs("foo", "field", 4, STRING, "neo", 0, 0.0, false, "").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ListAddI(nil, "foo", "field", 4, "neo")
	if err != nil {
		t.Fatal("Database ListAddI non-moving failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal(
			"Database ListAddI non-moving: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test rm
	// ======================================================
	// rm not at end
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "")
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(5)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("foo", "field", 5).
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDDATA").
		WithArgs(4, "foo", "field", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err = db.ListRM(nil, "foo", "field", 4)
	if err != nil {
		t.Fatal("Database ListRM failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database ListRM moving: unfulfilled expectations:", err)
	}

	// rm at end
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"}).
		AddRow(STRING, "baz", 0, 0.0, false, "")
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ID", "ENTRYINDEX", "ITEMID"})
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("foo", "field", 5).
		WillReturnRows(itemRows)
	mock.ExpectCommit()
	err = db.ListRM(nil, "foo", "field", 4)
	if err != nil {
		t.Fatal("Database ListRM failed:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database ListRM non-moving: unfulfilled expectations:",
			err)
	}

	// ======================================================
	// Test maxIndex
	// ======================================================
	// Empty
	itemRows = sqlmock.NewRows([]string{"MAXID"})
	mock.ExpectBegin()
	mock.ExpectQuery("select max\\(ITEMINDEX\\) as MAXID from FIELDDATA").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	highIndex, err := db.ListMaxIndex(nil, "foo", "field")
	if err != nil {
		t.Fatal("Database ListMaxIndex failed:", err)
	}
	if highIndex != -1 {
		t.Fatal("Database ListMaxIndex index error:", highIndex)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("MaxInde: unfulfilled expectations:", err)
	}

	// Filled
	itemRows = sqlmock.NewRows([]string{"MAXID"}).
		AddRow(4)
	mock.ExpectBegin()
	mock.ExpectQuery("select max\\(ITEMINDEX\\) as MAXID from FIELDDATA").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	highIndex, err = db.ListMaxIndex(nil, "foo", "field")
	if err != nil {
		t.Fatal("Database ListMaxIndex failed:", err)
	}
	if highIndex != 4 {
		t.Fatal("Database ListMaxIndex index error:", highIndex)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("MaxIndex: unfulfilled expectations:", err)
	}

	// ======================================================
	// Test ListSetI
	// ======================================================
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(false)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL", "EMBED"})
	mock.ExpectQuery(
		"select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA").
		WithArgs("foo", "field", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", 4, STRING, "bar", 0, 0.0, false, "")
	mock.ExpectCommit()
	err = db.ListSetI(nil, "foo", "field", 4, "bar")
	if err != nil {
		t.Fatal("ListSetI failure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ListSetI: unfulfilled expectations:", err)
	}
}

func Test_checkAndCreateFields(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test for FIELDS row doesn't exist
	// Setup expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "field", false)
	mock.ExpectCommit()
	// run test
	err = db.checkAndCreateFields(nil, "foo", "field", false)
	if err != nil {
		t.Fatal("checkAndCreateFields failure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("checkAndCreateFields unfulfilled expectations:", err)
	}

	// Test for FIELDS row same as desired
	// Setup expectations
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(false)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectCommit()
	// run test
	err = db.checkAndCreateFields(nil, "foo", "field", false)
	if err != nil {
		t.Fatal("checkAndCreateFields failure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("checkAndCreateFields unfulfilled expectations:", err)
	}

	// Test for FIELDS row does not equal desired
	// Setup expectations
	mock.ExpectBegin()
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(true)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("foo", "field").
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDS").
		WithArgs(false, "foo", "field").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()
	// run test
	err = db.checkAndCreateFields(nil, "foo", "field", false)
	if err != nil {
		t.Fatal("checkAndCreateFields failure:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("checkAndCreateFields unfulfilled expectations:", err)
	}
}
