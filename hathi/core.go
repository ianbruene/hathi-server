package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/dbase"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

const HATHI_MAJOR_VERSION = 0
const HATHI_MINOR_VERSION = 1
const HATHI_BUGFIX_VERSION = 0

var Logging bool

var Engine *Core

type OutboxPosting struct {
	ActorID  string
	OutboxID string
	Activity activitypub.Object
	Actor    activitypub.Object
	Prop     []string // Propagation
	Spawned  map[string]*activitypub.Object
	ReplyTos []string
	ReplyID  string // Hold the ID of the correct reply Object
}

type propagationRequest struct {
	Obj     *activitypub.Object
	Embeds  map[string]*activitypub.Object
	Targets []string
}

type targetNode struct {
	Id        string
	RetryTime time.Time
	Tries     uint8
}

type queueNode struct {
	Obj     *activitypub.Object // The object we need to push
	Embeds  map[string]*activitypub.Object
	Targets []targetNode // The initial propagation targets. Could be anything.
	Inboxes []targetNode // The actual inboxes we need to post to.
	Seen    []string
	Encoded []byte // So we don't encode this over and over.
}

type DumpData struct {
	Objects    []*activitypub.Object
	Privileges map[string]map[string][]string
	DumpInfo   struct {
		MajorVersion  uint
		MinorVersion  uint
		BugFixVersion uint
		DumpTime      string
	}
}

// Core is the root object
type Core struct {
	nextID    uint64
	LocalURL  string
	LocalPort string
	db        *dbase.HathiDB
	// Remote propagation
	objectQueues map[string]queueNode
	propRequests chan propagationRequest
}

func InitEngine(url string, port string, dbfile string) (err error) {
	isNew := false
	Engine = new(Core)
	Engine.LocalURL = url
	Engine.LocalPort = port
	if _, err = os.Stat(dbfile); os.IsNotExist(err) {
		isNew = true
	}
	Engine.db, err = dbase.OpenDatabase("sqlite3", dbfile)
	if err != nil {
		return err
	}
	if isNew {
		Engine.db.Initialize()
		if err != nil {
			return err
		}
	}
	Engine.objectQueues = make(map[string]queueNode)
	Engine.propRequests = make(chan propagationRequest, 16) // Arbitrary size
	return nil
}

func (c *Core) GetObject(objID string) (obj *activitypub.Object, err error) {
	return c.db.LoadObject(nil, objID)
}

func (c *Core) NewID() (id string) {
	// Generates a numeric ID
	localid := c.nextID
	c.nextID++
	return fmt.Sprintf("http://%s:%s/%x", c.LocalURL, c.LocalPort, localid)
}

func (c *Core) MakeIDFromLocalID(localid string) (id string) {
	// Generates a specific ID from a base
	// Used for actor and special object IDs
	if Logging {
		log.Println("Building ID from:", localid)
	}
	for localid[0] == '/' {
		localid = strings.TrimPrefix(localid, "/")
	}
	for localid[len(localid)-1] == '/' {
		localid = strings.TrimSuffix(localid, "/")
	}
	return fmt.Sprintf("http://%s:%s/%s", c.LocalURL, c.LocalPort, localid)
}

func (c *Core) IsIDRemote(id string) (remoteP bool, err error) {
	url, err := url.Parse(id)
	if err != nil {
		return false, err
	}
	hostMatch := url.Hostname() == c.LocalURL
	portMatch := false
	if (url.Port() == "") && (c.LocalPort == "8080") {
		portMatch = true
	} else if url.Port() == c.LocalPort {
		portMatch = true
	}
	if hostMatch && portMatch {
		return false, nil
	} else {
		return true, nil
	}
}

func (c *Core) AddActor(tx *sql.Tx, actorID string, actorType string) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	// Build actor
	actor := activitypub.NewObject()
	actor.SetID(actorID)
	actor.SetField("@type", actorType)
	// Setup collections
	in := activitypub.NewObject()
	in.SetID(actor.GetID() + "/inbox")
	// These will be removed when init / helpers are complete
	in.SetField("@type", "OrderedCollection")
	actor.SetField("inbox", in.Id)

	out := activitypub.NewObject()
	out.SetID(actor.GetID() + "/outbox")
	out.SetField("@type", "OrderedCollection")
	actor.SetField("outbox", out.Id)

	followers := activitypub.NewObject()
	followers.SetID(actor.GetID() + "/followers")
	followers.SetField("@type", "OrderedCollection")
	actor.SetField("followers", followers.Id)

	following := activitypub.NewObject()
	following.SetID(actor.GetID() + "/following")
	following.SetField("@type", "OrderedCollection")
	actor.SetField("following", following.Id)
	// Commit
	err = c.db.StoreObject(tx, actor)
	if err != nil {
		return err
	}
	err = c.db.StoreObject(tx, in)
	if err != nil {
		return err
	}
	err = c.db.StoreObject(tx, out)
	if err != nil {
		return err
	}
	err = c.db.StoreObject(tx, followers)
	if err != nil {
		return err
	}
	err = c.db.StoreObject(tx, following)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) HandleReply(tx *sql.Tx, replyID string, targetID string) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	// TODO: remote replies
	// TODO: propogation to inboxes
	// Get the target
	target, err := c.db.LoadObject(tx, targetID)
	if err != nil {
		return err
	}
	if target == nil {
		// No target, bail
		return nil
	}
	// Get the replies collection, or make one if it doesn't exist
	replies, _ := target.GetFieldAsString("replies")
	if replies == "" {
		// no reply collection yet, make one
		repliesCol := activitypub.NewObject()
		repliesCol.SetField("@type", "Collection")
		repliesCol.SetField("items", []string{})
		repliesCol.SetID(targetID + "/replies")
		err = c.db.StoreObject(tx, repliesCol)
		if err != nil {
			return nil
		}
		target.SetField("replies", repliesCol.Id)
		err = c.db.StoreObject(tx, target)
		if err != nil {
			return err
		}
	}
	// Assign
	err = c.db.ListAppend(tx, target.GetField("replies").(string), "items", replyID)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) Propagate(tx *sql.Tx, a *activitypub.Object, targetIDs []string) {
	// TODO: Add propogation for replies
	targetIDs = preparePropList(targetIDs, a.GetField("origin").(string))
	remoteList := []string{}
	for _, id := range targetIDs {
		actor, _ := c.db.LoadObject(tx, id)
		if actor == nil {
			continue
		}
		if !actor.IsActor() {
			continue
		}
		c.HandleInboxActivity(tx, false, actor.GetField("inbox").(string),
			id, a)
	}
	if len(remoteList) > 0 {
		c.RequestRemotePropagation(a, remoteList)
	}
}

func (c *Core) RequestRemotePropagation(a *activitypub.Object, targetIDs []string) {
	req := propagationRequest{Obj: a, Targets: targetIDs}
	c.propRequests <- req
}

func (c *Core) DoRemotePropagation() {
	for true {
		// Check for prop requests and add
		select {
		case req := <-c.propRequests:
			objID := req.Obj.GetID()
			if _, ok := c.objectQueues[objID]; ok {
				// Already exists...... what to do?
			} else {
				node := queueNode{Obj: req.Obj}
				node.Targets = make([]targetNode, len(req.Targets))
				for i, target := range req.Targets {
					node.Targets[i].Id = target
				}
				c.objectQueues[objID] = node
			}
		default:
		}
		for id, queue := range c.objectQueues {
			if len(queue.Targets) > 0 {
				getObjectsForQueue(&queue)
			}
			if len(queue.Inboxes) > 0 {
				pushInboxesFromQueue(&queue)
			}
			if len(queue.Targets) == 0 && len(queue.Inboxes) == 0 {
				// We don't have anything left to propagate, remove the queue
				delete(c.objectQueues, id)
			}
		}
		// replace with more sophisticated limiter later
		time.Sleep(100 * time.Millisecond)
	}
}

func attemptObjectGet(target targetNode) (newTargets []targetNode, newInboxes []targetNode, seen bool) {
	newTargets = make([]targetNode, 0) // Targets we need to try next time
	newInboxes = make([]targetNode, 0) // Inboxes we have found and can pass on
	resp, err := http.Get(string(target.Id))
	if err != nil {
		// connection failed, should expand the checks later
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, false
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return []targetNode{target}, newInboxes, false
	}
	defer resp.Body.Close()
	rawData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// data read failed, should expand the checks later
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, true
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return newTargets, newInboxes, false
	}
	status := resp.StatusCode
	if status == http.StatusNotFound {
		// Target doesn't exist
		return []targetNode{}, []targetNode{}, true
	} else if status != http.StatusOK {
		// Unspecified status code
		// drop for now
		return []targetNode{}, []targetNode{}, true
	}

	// Ok, by this point we should have an object
	targetObj, err := activitypub.DecodeJSON(rawData)
	if err != nil {
		// JSON error, retry
		t := getNextRetryTime(target.Tries)
		if t.IsZero() { // Too many retries
			return []targetNode{}, []targetNode{}, false
		}
		target.Tries++
		newTargets = append(newTargets, target)
		return newTargets, newInboxes, false
	}

	if targetObj.IsActor() {
		// target is an Actor, get its inbox
		newInboxes = append(newInboxes,
			targetNode{targetObj.GetField("inbox").(string), time.Time{}, 0})
		// Handled this Actor, push it to seen
		return newTargets, newInboxes, true
	} else if targetObj.IsCollection() {
		// target is a list of objects, push them to the target list
		for _, temp := range targetObj.GetField("items").([]interface{}) {
			id := temp.(string)
			newNode := targetNode{id, time.Time{}, 0}
			newTargets = append(newTargets, newNode)
		}
		// Handled this collection, push it to seen
		return newTargets, newInboxes, true
	}
	return []targetNode{}, []targetNode{}, false
}

func getObjectsForQueue(q *queueNode) {
	currentTime := time.Now()
	newTargets := make([]targetNode, 0) // Targets we need to try next time
	newInboxes := make([]targetNode, 0) // Inboxes we have found and can pass on
TARGET:
	for _, target := range q.Targets {
		for _, s := range q.Seen { // De-duplicate
			if s == target.Id {
				continue TARGET
			}
		}
		if target.RetryTime.IsZero() || currentTime.After(target.RetryTime) {
			// Expired, time to retry
			tgts, inboxes, seen := attemptObjectGet(target)
			newTargets = append(newTargets, tgts...)
			newInboxes = append(newInboxes, inboxes...)
			if seen {
				q.Seen = append(q.Seen, target.Id)
			}
		}
	}
	q.Targets = newTargets
	q.Inboxes = newInboxes
}

func getNextRetryTime(tries uint8) (t time.Time) {
	// 1min, 10min, 30min, 90min (repeat)
	// We get the try count before increment
	var inc time.Duration
	switch tries {
	case 0:
		inc = time.Duration(time.Minute)
	case 1:
		inc = time.Duration(time.Minute * 10)
	case 2:
		inc = time.Duration(time.Minute * 30)
	case 255: // Too many retries, bail
		return time.Time{}
	default:
		inc = time.Duration(time.Minute * 90)
	}
	t = time.Now()
	t = t.Add(inc)
	return t
}

func pushInboxesFromQueue(q *queueNode) {
	// Fill q.Encoded with encoded and embedded object
	if q.Encoded == nil {
		enc := q.Obj.Mapify()
		q.Encoded, _ = json.Marshal(enc)
	}
	newInboxes := make([]targetNode, 0)
	currentTime := time.Now()
	for _, target := range q.Inboxes {
		if target.RetryTime.IsZero() || currentTime.After(target.RetryTime) {
			req, err := http.NewRequest("POST", string(target.Id),
				bytes.NewBuffer(q.Encoded))
			if err != nil {
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
			req.Header.Set("Content-Type", "application/json")
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
			defer resp.Body.Close()
			status := resp.StatusCode
			if status == http.StatusCreated {
				// Sucess
				resp.Body.Close()
				continue
			} else {
				// Error
				t := getNextRetryTime(target.Tries)
				if t.IsZero() { // Too many retries
					continue
				}
				target.Tries++
				target.RetryTime = t
				newInboxes = append(newInboxes, target)
				continue
			}
		}
	}
	q.Inboxes = newInboxes
}

func (c *Core) DumpDB() (dump *DumpData, err error) {
	dump = new(DumpData)
	tx, err := c.db.BeginTransaction()
	if err != nil {
		return nil, err
	}
	defer c.db.EndTransaction(tx, &err)
	// Suck out the objects
	objIDs, err := c.db.GetAllObjectIDs(tx)
	if err != nil {
		return nil, err
	}
	dump.Objects = make([]*activitypub.Object, len(objIDs))
	for index, objID := range objIDs {
		obj, err := c.db.LoadObject(tx, objID)
		if err != nil {
			return nil, err
		}
		dump.Objects[index] = obj
	}
	// Suck out the privileges
	privIDs, err := c.db.GetAllPrivilegeIDs(tx)
	if err != nil {
		return nil, err
	}
	dump.Privileges = make(map[string]map[string][]string, 0)
	for _, privID := range privIDs {
		priv, err := c.db.GetObjectPrivileges(tx, privID)
		if err != nil {
			return nil, err
		}
		dump.Privileges[privID] = priv
	}
	// Misc data
	// TODO: change to major, minor, bugfix uints
	dump.DumpInfo.MajorVersion = HATHI_MAJOR_VERSION
	dump.DumpInfo.MinorVersion = HATHI_MINOR_VERSION
	dump.DumpInfo.BugFixVersion = HATHI_BUGFIX_VERSION
	t := time.Now()
	dump.DumpInfo.DumpTime = t.Format(time.RFC3339)
	return dump, nil
}

func (c *Core) LoadDB(dump *DumpData) (err error) {
	tx, err := c.db.BeginTransaction()
	if err != nil {
		return err
	}
	defer c.db.EndTransaction(tx, &err)
	// Inject objects
	for _, obj := range dump.Objects {
		err = c.db.StoreObject(tx, obj)
		if err != nil {
			return err
		}
	}
	// Inject privileges
	for privID, privs := range dump.Privileges {
		err = c.db.SetObjectPrivileges(tx, privID, privs)
		if err != nil {
			return err
		}
	}
	return nil
}

// ================================
// New stuff below this
// ================================

func (c *Core) DeleteObject(tx *sql.Tx, objID string) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	// Attempt to load object
	obj, err := c.db.LoadObject(tx, objID)
	if err != nil {
		return err
	}
	if obj == nil {
		return errors.New("Can't delete non-existing object")
	}
	objType, _ := obj.GetFieldAsString("@type")
	if objType == "Tombstone" {
		return errors.New("Can't delete Tombstone")
	}
	// Carve Tombstone
	tomb := c.carveTombstone(obj)
	// Delete original object
	err = c.db.DeleteObjectData(tx, objID)
	if err != nil {
		return err
	}
	// Erect Tombstone
	err = c.db.StoreObject(tx, tomb)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) carveTombstone(memorialized *activitypub.Object) (tomb *activitypub.Object) {
	tomb = activitypub.NewObject()
	tomb.SetID(memorialized.GetID())
	tomb.SetField("@type", "Tombstone")
	tomb.SetField("formerType", memorialized.GetField("@type"))
	currentTime := time.Now()
	tomb.SetField("deleted", currentTime.Format(time.RFC3339))
	temp := memorialized.GetField("to")
	if temp != nil {
		tomb.SetField("to", temp)
	}
	temp = memorialized.GetField("bto")
	if temp != nil {
		tomb.SetField("bto", temp)
	}
	temp = memorialized.GetField("cc")
	if temp != nil {
		tomb.SetField("cc", temp)
	}
	temp = memorialized.GetField("bcc")
	if temp != nil {
		tomb.SetField("bcc", temp)
	}
	temp = memorialized.GetField("audience")
	if temp != nil {
		tomb.SetField("audience", temp)
	}
	return tomb
}
