package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"database/sql"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
)

// The outbox POST handler calls HandleOutboxActivity to deal with objects
// it recieves. This method deals with distributing the activity to the
// proper handler, storing the Activity in the database, and adding a
// reference to the outbox collection.
// Propagation and side effects are handled by the activity-specific handlers.
func (c *Core) HandleOutboxActivity(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object) (objectID string, err error) {
	actorID := actor.GetID()
	activity.SetField("@id", c.NewID())
	activityType, _ := activity.GetFieldAsString("@type")
	activity.SetField("origin", actorID) // <-- this might be wrong
	// Do side effects and propagations for most Activities.
	// Do all the handling for special Activities like Create.
	prop := []string{}
	switch activityType {
	case "Accept":
		err = c.HandleOutAccept(tx, activity, actor, prop)
	case "Add":
	case "Announce":
	case "Arrive":
	case "Block":
	case "Create":
		err = c.HandleOutCreate(tx, activity, actor, prop)
	case "Delete":
		err = c.HandleOutDelete(tx, activity, actor, prop)
	case "Dislike":
	case "Flag":
	case "Follow":
		err = c.HandleOutFollow(tx, activity, actor, prop)
	case "Ignore":
	case "Invite":
	case "Join":
	case "Leave":
	case "Like":
	case "Listen":
	case "Move":
	case "Offer":
	case "Question":
	case "Reject":
		err = c.HandleOutReject(tx, activity, actor, prop)
	case "Read":
	case "Remove":
	case "TentativeReject":
	case "TentativeAccept":
		err = c.HandleOutTentativeAccept(tx, activity, actor, prop)
	case "Travel":
	case "Undo":
	case "Update":
	case "View":
	}

	// Had an error; this covers errors both internally and in the submission
	if err != nil {
		return "", err
	}
	// Store the Activity in the database
	c.db.StoreObject(tx, activity)
	if err != nil {
		return "", err
	}
	activityID := activity.GetID()
	c.db.SetPrivilege(tx, activityID, actorID, "owner", true)
	// Distribute replies
	replyList, _ := activity.GetFieldAsStringList("inReplyTo")
	if replyList != nil {
		for _, replyTarget := range replyList {
			isRemote, err := c.IsIDRemote(replyTarget)
			if err != nil {
				continue // Skip this?
			}
			if isRemote == false {
				c.HandleReply(tx, activityID, replyTarget)
			}
		}
	}
	// Do standard propagation + extras
	temp, _ := activity.GetFieldAsStringList("to")
	if temp != nil {
		prop = append(prop, temp...)
	}
	temp, _ = activity.GetFieldAsStringList("bto")
	if temp != nil {
		prop = append(prop, temp...)
	}
	temp, _ = activity.GetFieldAsStringList("cc")
	if temp != nil {
		prop = append(prop, temp...)
	}
	temp, _ = activity.GetFieldAsStringList("bcc")
	if temp != nil {
		prop = append(prop, temp...)
	}
	temp, _ = activity.GetFieldAsStringList("audience")
	if temp != nil {
		prop = append(prop, temp...)
	}
	c.Propagate(tx, activity, prop)
	// We handle adding to the outbox, the activity handlers handle other
	// side effects
	outboxID, _ := actor.GetFieldAsString("outbox")
	c.db.ListAppend(tx, outboxID, "items", activityID)
	return activity.GetID(), nil
}

// Temporary function
func i2HList(a []interface{}) (b []string) {
	b = make([]string, len(a))
	for i, v := range a {
		b[i] = v.(string)
	}
	return b
}
