package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"gitlab.com/hathi-social/hathi-server/dbase"
	"log"
)

func (c *Core) PostToOutbox(actorID string, event *activitypub.Object) (objID string, externalError string, err error) {
	if Logging {
		log.Println("Posting to Outbox:", actorID, event)
	}
	// Begin transaction
	tx, err := c.db.BeginTransaction()
	defer c.db.EndTransaction(tx, &err)
	if err != nil {
		return "", "Internal server error", err
	}
	// Check Actor existence
	actor, err := c.db.LoadObject(tx, actorID)
	if err != nil {
		return "", "Internal server error", err
	}
	if actor == nil {
		if Logging {
			log.Println("Outbox POST error: invalid Actor ID", actorID)
		}
		return "", "Actor doesn't exist", err
	} else if actor.IsActor() != true {
		return "", "Actor doesn't exist", err
	}
	// Check for bare object
	if event.IsActivity() == false {
		event = makeCreateWrapper(actorID, event)
	}
	// Run handlers
	objectID, err := c.HandleOutboxActivity(tx, event, actor)
	if err == nil {
		return objectID, "", nil
	} else {
		if Logging {
			log.Println("Outbox POST handling error;", err)
		}
		return "", "Internal server error", err
	}
}

func (c *Core) PostToInbox(actorID string, event *activitypub.Object) (externalError string, err error) {
	// loop through types
	// for each type we support: call its inbox handler
	// if remote store cached version
	return "", nil
}

func (c *Core) RetrieveObject(objID string, actor string) (obj *activitypub.Object, externalError string, err error) {
	// WIP: minimum function, add checks once it works
	// TODO: Check object ID for actor ownership
	// Load object
	obj, err = c.db.LoadObject(nil, objID)
	if err != nil {
		return nil, "Internal server error", err
	}
	// if not owning actor, check propagation fields
	// if allowed, return object
	return obj, "", nil
}

func (c *Core) CreateActor(actorID string) (externalError string, location string, err error) {
	// Not quite sure about what data will be needed. This is a stopgap.
	// Because it is a stopgap it still has bad seperation from HTTP
	if Logging {
		log.Println("Creating actor:", actorID)
	}
	// Try to load existing object
	obj, err := c.db.LoadObject(nil, actorID)
	if err != nil {
		return "Internal server error", "", err
	}
	if obj != nil {
		// Actor already exists, fail
		return "Actor already exists", "", nil
	}
	// TODO: different actor types
	// Store Actor
	err = c.AddActor(nil, actorID, "Person")
	return "", actorID, nil
}

func (c *Core) DeleteActor(actorID string) (externalError string, err error) {
	if Logging {
		log.Println("Deleting Actor:", actorID)
	}
	err = c.DeleteObject(nil, actorID)
	if err == nil {
		// No error
		return "", nil
	} else if err.Error() == "Can't delete non-existing object" {
		// no object
		if Logging {
			log.Println("Attempted deletion non-existant Actor")
		}
		return "Actor does not exist", err
	} else if err.Error() == "Can't delete Tombstone" {
		// Already deleted object
		if Logging {
			log.Println("Attempted deletion of Tombstone")
		}
		return "Can't delete Tombstone", err
	} else {
		// Other error
		return "Internal server error", err
	}
}
