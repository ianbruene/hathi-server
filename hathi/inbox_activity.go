package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
	"database/sql"
)

func (c *Core) HandleInAccept(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	// Figure out how to handle remotes
	// Need to know the type of object we are accepting
	temp, _ := act.GetFieldAsString("object")
	acptObj, err := c.db.LoadObject(tx, temp)
	if err != nil {
		return err
	}
	temp, _ = acptObj.GetFieldAsString("@type")
	switch temp {
	case "Follow":
		// Add followee, we need to get the actor's following collection
		temp, _ = acptObj.GetFieldAsString("origin")
		follower, err := c.db.LoadObject(tx, temp)
		if err != nil {
			return err
		}
		followTargetID, _ := act.GetFieldAsString("origin")
		// Add the who is being followed to the follower's following list
		followingListID, _ := follower.GetFieldAsString("following")
		c.db.ListAppend(tx, followingListID, "items", followTargetID)
	}
	return nil
}

func (c *Core) HandleInAdd(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInAnnounce(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInArrive(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInBlock(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInCreate(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	if remoteP == true {
		subObj := act.GetField("object").(*activitypub.Object)
		subObj.SetID(c.NewID())
	}
	return nil
}

func (c *Core) HandleInDelete(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInDislike(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInFlag(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInFollow(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	// Nothing to do here
	return nil
}

func (c *Core) HandleInIgnore(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInInvite(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInJoin(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInLeave(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInLike(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInListen(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInMove(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInOffer(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInQuestion(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInReject(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	// Figure out how to handle remotes
	// Need to know the type of object we are rejecting
	acptObj, err := c.db.LoadObject(tx, act.GetField("object").(string))
	if err != nil {
		return err
	}
	switch acptObj.GetField("type").(string) {
	case "Follow":
	}
	return nil
}

func (c *Core) HandleInRead(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInRemove(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInTentativeReject(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInTentativeAccept(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return c.HandleInAccept(tx, remoteP, act)
}

func (c *Core) HandleInTravel(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInUndo(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInUpdate(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}

func (c *Core) HandleInView(tx *sql.Tx, remoteP bool, act *activitypub.Object) (err error) {
	return nil
}
