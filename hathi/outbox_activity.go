package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"database/sql"
	"fmt"
	"gitlab.com/hathi-social/hathi-server/activitypub"
	"time"
)

func (c *Core) HandleOutAccept(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	// Need to know the type of object we are accepting
	temp, _ := activity.GetFieldAsString("object")
	acptObj, err := c.db.LoadObject(tx, temp)
	if err != nil {
		return err
	}
	temp, _ = acptObj.GetFieldAsString("@type")
	switch temp {
	case "Follow":
		// Add follower, we need to get the actor's followers Collection
		temp, _ = activity.GetFieldAsString("origin")
		followTarget, err := c.db.LoadObject(tx, temp)
		if err != nil {
			return err
		}
		followerID, _ := acptObj.GetFieldAsString("origin")
		// Add the actor who is following to the target's followers
		followersListID, _ := followTarget.GetFieldAsString("followers")
		c.db.ListAppend(tx, followersListID, "items", followerID)
		prop = append(prop, followerID)
	}
	return nil
}

func (c *Core) HandleOutAdd(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutAnnounce(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutArrive(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutBlock(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutCreate(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	actorID := actor.GetID()
	obj := activity.GetField("object").(*activitypub.Object)
	obj.SetID(c.NewID())
	// Sync propagation addresses
	act := activity
	// to
	actList, _ := act.GetFieldAsStringList("to")
	objList, _ := obj.GetFieldAsStringList("to")
	syncLists(&actList, &objList, true)
	act.SetField("to", actList)
	obj.SetField("to", objList)
	// bto
	actList, _ = act.GetFieldAsStringList("bto")
	objList, _ = obj.GetFieldAsStringList("bto")
	syncLists(&actList, &objList, true)
	act.SetField("bto", actList)
	obj.SetField("bto", objList)
	// cc
	actList, _ = act.GetFieldAsStringList("cc")
	objList, _ = obj.GetFieldAsStringList("cc")
	syncLists(&actList, &objList, true)
	act.SetField("cc", actList)
	obj.SetField("cc", objList)
	// bcc
	actList, _ = act.GetFieldAsStringList("bcc")
	objList, _ = obj.GetFieldAsStringList("bcc")
	syncLists(&actList, &objList, true)
	act.SetField("bcc", actList)
	obj.SetField("bcc", objList)
	// audience
	actList, _ = act.GetFieldAsStringList("audience")
	objList, _ = obj.GetFieldAsStringList("audience")
	syncLists(&actList, &objList, true)
	act.SetField("audience", actList)
	obj.SetField("audience", objList)
	// Set attributedTo
	act.SetField("attributedTo", actorID)
	obj.SetField("attributedTo", actorID)
	// Do reply stuff
	replyList, _ := obj.GetFieldAsStringList("inReplyTo")
	if replyList != nil {
		for _, replyTarget := range replyList {
			c.HandleReply(tx, obj.GetID(), replyTarget)
		}
	}
	// Commit
	c.db.SetPrivilege(tx, obj.GetID(), actorID, "owner", true)
	c.db.StoreObject(tx, obj)
	return nil
}

func (c *Core) HandleOutDelete(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	actorID := actor.GetID()
	// Check that this actor is allowed to delete the object
	allowed, err := c.db.CheckPrivilege(tx,
		activity.GetField("object").(string),
		activity.GetField("origin").(string),
		"owner")
	if err != nil {
		return err
	}
	if allowed == false {
		return fmt.Errorf("Not allowed: Actor %s does not own Object %s",
			actorID, activity.GetField("object"))
	}
	// Get target object
	oldObj, err := c.db.LoadObject(tx, activity.GetField("object").(string))
	if err != nil {
		return err
	}
	if oldObj == nil { // Object does not exist
		return nil
	}
	// Get original propagation, we will inform the target object's original
	// targets of the deletion.
	act := activity
	// to
	oldList, _ := oldObj.GetFieldAsStringList("to")
	actList, _ := act.GetFieldAsStringList("to")
	syncLists(&oldList, &actList, false)
	act.SetField("to", actList)
	oldObj.SetField("to", oldList)
	// bto
	oldList, _ = oldObj.GetFieldAsStringList("bto")
	actList, _ = act.GetFieldAsStringList("bto")
	syncLists(&oldList, &actList, false)
	act.SetField("bto", actList)
	oldObj.SetField("bto", oldList)
	// cc
	oldList, _ = oldObj.GetFieldAsStringList("cc")
	actList, _ = act.GetFieldAsStringList("cc")
	syncLists(&oldList, &actList, false)
	act.SetField("cc", actList)
	oldObj.SetField("cc", oldList)
	// bcc
	oldList, _ = oldObj.GetFieldAsStringList("bcc")
	actList, _ = act.GetFieldAsStringList("bcc")
	syncLists(&oldList, &actList, false)
	act.SetField("bcc", actList)
	oldObj.SetField("bcc", oldList)
	// audience
	oldList, _ = oldObj.GetFieldAsStringList("audience")
	actList, _ = act.GetFieldAsStringList("audience")
	syncLists(&oldList, &actList, false)
	act.SetField("audience", actList)
	oldObj.SetField("audience", oldList)
	// Create replacement tombstone
	currentTime := time.Now()
	tomb := activitypub.NewObject()
	tomb.SetID(oldObj.GetID())
	tomb.SetField("@type", "Tombstone")
	tomb.SetField("formerType", oldObj.GetField("@type"))
	tomb.SetField("deleted", currentTime.Format(time.RFC3339))
	temp := activity.GetField("to")
	if temp != nil {
		tomb.SetField("to", temp)
	}
	temp = activity.GetField("bto")
	if temp != nil {
		tomb.SetField("bto", temp)
	}
	temp = activity.GetField("cc")
	if temp != nil {
		tomb.SetField("cc", temp)
	}
	temp = activity.GetField("bcc")
	if temp != nil {
		tomb.SetField("bcc", temp)
	}
	temp = activity.GetField("audience")
	if temp != nil {
		tomb.SetField("audience", temp)
	}
	// Replace object with tombstone
	c.db.StoreObject(tx, tomb)
	c.db.SetPrivilege(tx, tomb.GetID(), actorID, "owner", true)
	return nil
}

func (c *Core) HandleOutDislike(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutFlag(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutFollow(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	temp, _ := activity.GetFieldAsString("object")
	prop = append(prop, temp)
	// Do side effects
	return nil
}

func (c *Core) HandleOutIgnore(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutInvite(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutJoin(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutLeave(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutLike(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	activity.SetField("@id", c.NewID())
	prop = append(prop, activity.GetField("object").(string))
	// Add to our "liked" collection
	return nil
}

func (c *Core) HandleOutListen(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutMove(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutOffer(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutQuestion(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutReject(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	// Need to know the type of object we are rejecting
	temp, _ := activity.GetFieldAsString("object")
	acptObj, err := c.db.LoadObject(tx, temp)
	if err != nil {
		return err
	}
	temp, _ = acptObj.GetFieldAsString("@type")
	switch temp {
	case "Follow":
	}
	return nil
}

func (c *Core) HandleOutRead(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutRemove(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutTentativeReject(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutTentativeAccept(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return c.HandleOutAccept(tx, activity, actor, prop)
}

func (c *Core) HandleOutTravel(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutUndo(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutUpdate(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}

func (c *Core) HandleOutView(tx *sql.Tx, activity *activitypub.Object, actor *activitypub.Object, prop []string) (err error) {
	return nil
}
