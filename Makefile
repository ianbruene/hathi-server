
#
# Makefile for hathi-server
#

build:
	go build server.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
