package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"strings"
	//"fmt"
)

type ValidationReport struct {
	MissingContext      bool   // No @context field
	InvalidContext      bool   // @context field invalid type
	MissingVocab        bool   // No vocabulary reference found
	ImproperVocab       bool   // ActivityPub vocabulary not used
	DefaultLanguage     string // Contents of the @language field
	NoType              bool   // No type field detected
	TypeAlias           bool   // Using "type" instead of "@type"
	ContainsInvalidType bool
	NoId                bool // No id field detected
	IdAlias             bool // Using "id" instead of "@id"
	InvalidFieldNames   []string
	UnknownFieldNames   []string // Unaliased fields that are not recognized
	BadFieldTypes       []string // Known fields with invalid datatypes
}

func FieldType(candidate string) (iri bool, compact bool, term bool) {
	colon := false
	slash := false
	// contains ':'?
	if index := strings.LastIndex(candidate, ":"); index >= 0 {
		colon = true
		// is that immediately folloewd by '//'?
		if strings.Index(candidate, "//") == (index + 1) {
			slash = true
		}
	}
	if colon == false && slash == false {
		return false, false, true
	} else if colon == true && slash == false {
		return false, true, false
	} else if colon == true && slash == true {
		return true, false, false
	} else {
		// can't happen
		return false, false, false
	}
}

func ExpandTerm(term string) (implemented bool, IRI string) {
	IRI, implemented = BuiltInFields[term]
	if implemented == false {
		IRI, implemented = BuiltInObjectTypes[term]
	}
	return implemented, IRI
}

func ExpandCompact(compact string, aliases map[string]string) (implemented bool, IRI string, typeDescriptor *TypeNode) {
	// Returning an empty IRI means invalid
	// split alias and field
	parts := strings.Split(compact, ":")
	if len(parts) != 2 {
		return false, "", nil
	}
	alias := parts[0]
	field := parts[1]
	if alias == "as" {
		// "as" is a standard alias for the activitystreams core
		implemented, IRI = ExpandTerm(field)
		typeDescriptor = BuiltinType(IRI)
		return implemented, IRI, typeDescriptor
	}
	// check defined aliases
	if root, ok := aliases[alias]; ok == true {
		// Alias has been defined
		IRI = root + "#" + field
	} else {
		// Undefined alias...
		return false, "", nil
	}
	// We definately have an IRI by now, check for implementation
	temp, implemented := ExtensionFieldTypes[IRI]
	if implemented {
		typeDescriptor = &temp
	} else {
		typeDescriptor = nil
	}
	return implemented, IRI, typeDescriptor
}

func validateValueSingle(value interface{}, typeDescriptor *TypeNode) (validType bool) {
	// Loop through the possible types trying to get a match
	for _, typeD := range typeDescriptor.Types {
		// get the type handler for this type
		validator, ok := TypeValidators[typeD]
		if ok {
			// call the handler with the value
			validType = validator(value)
			if validType == true {
				// found a lock
				return validType
			}
		}
	}
	// Didn't find any lock; invalid
	return false
}

func ValidateValue(value interface{}, typeDescriptor *TypeNode) (validType bool) {
	if typeDescriptor == nil {
		return true // We can't test this, just pass it
	}
	switch typedV := value.(type) {
	case []interface{}:
		if typeDescriptor.Multi == false {
			return false // This field is not allowed to be a list
		} else {
			// Handle list
			for _, item := range typedV {
				// loop through each item, validate it's type
				validType := validateValueSingle(item, typeDescriptor)
				if validType == false {
					// This item is not valid.
					return false
				}
			}
			// if all items are valid then the whole is valid
			return true
		}
	default:
		return validateValueSingle(value, typeDescriptor)
	}
	return false
}

func ValidateFieldName(field string, aliases map[string]string) (implemented bool, valid bool, internalName string, typeDescriptor *TypeNode) {
	iri, compact, term := FieldType(field)
	if iri == false && compact == false && term == false {
		// Can't happen
		return false, false, "", nil
	}
	valid = false
	typeDescriptor = nil
	if term == true {
		implemented, internalName = ExpandTerm(field)
		if implemented == true {
			typeDescriptor = BuiltinType(internalName)
			valid = true
		}
	} else if compact == true {
		// expand compact
		implemented, internalName, typeDescriptor = ExpandCompact(field,
			aliases)
		if internalName != "" {
			valid = true
		}
	} else if iri == true {
		internalName = ClipIRI(field)
		valid = true
		// Look for a type descriptor for this IRI
		typeDescriptor = BuiltinType(internalName)
		if typeDescriptor == nil {
			typeTemp, ok := ExtensionFieldTypes[internalName]
			if ok {
				typeDescriptor = &typeTemp
			} else {
				typeDescriptor = nil
			}
		}
	}
	// At this point we have an IRI, either directly or through expansion
	return implemented, valid, internalName, typeDescriptor
}

func ValidateField(name string, value interface{}, aliases map[string]string) (validName bool, implementedName bool, validType bool) {
	implementedName, validName, _, typeD := ValidateFieldName(name, aliases)
	if implementedName == true {
		validType = ValidateValue(value, typeD)
	} else {
		validType = true // We cannot know
	}
	return validName, implementedName, validType
}

func ValidateType(data interface{}, aliases map[string]string) (valid bool) {
	switch typedV := data.(type) {
	case string:
		return validateTypeSingle(typedV, aliases)
	case []interface{}:
		for _, item := range typedV {
			switch itemV := item.(type) {
			case string:
				valid = validateTypeSingle(itemV, aliases)
				if valid == false {
					return false
				}
			default:
				return false
			}
		}
		// All list items are valid
		return true
	default:
		return false
	}
}

func validateTypeSingle(item string, aliases map[string]string) (valid bool) {
	iri, comp, term := FieldType(item)
	if iri == true {
		valid = validateIRI(item)
	} else if comp == true {
		_, iri, _ := ExpandCompact(item, aliases)
		valid = (iri != "") // Empty IRI means invalid
	} else if term == true {
		_, valid = BuiltInObjectTypes[item]
	} else {
		// Nothing is true, can't happen
		valid = false
	}
	return valid
}

func ValidateContext(data interface{}, report *ValidationReport) (aliases map[string]string) {
	aliases = make(map[string]string, 0)
	switch typedV := data.(type) {
	case string:
		report.InvalidContext = isVocabInvalid(typedV)
		// There is only one item. If the context is invalid it *must* also be
		// an ImproperVocab
		report.ImproperVocab = report.InvalidContext
	case map[string]interface{}:
		invalid, noVocab, impVocab, defLang := validateContextObject(typedV,
			aliases)
		report.InvalidContext = invalid
		report.MissingVocab = noVocab
		report.ImproperVocab = impVocab
		report.DefaultLanguage = defLang
	case []interface{}:
		invalid, noVocab, impVocab, defLang := validateContextList(typedV,
			aliases)
		report.InvalidContext = invalid
		report.MissingVocab = noVocab
		report.ImproperVocab = impVocab
		report.DefaultLanguage = defLang
	default:
		report.InvalidContext = true
	}
	if report.InvalidContext == true {
		return nil // No aliases
	} else {
		return aliases
	}
}

func isVocabInvalid(vocab string) (inValid bool) {
	if vocab == "https://www.w3.org/ns/activitystreams" {
		return false
	} else if vocab == "http://www.w3.org/ns/activitystreams" {
		return false
	} else {
		return true
	}
}

func validateContextObject(obj map[string]interface{}, aliases map[string]string) (invalid bool, noVocab bool, impVocab bool, defLang string) {
	invalid = false
	noVocab = true
	defLang = ""
	for field, value := range obj {
		switch typedV := value.(type) {
		case string:
			switch field {
			case "@vocab":
				noVocab = false
				impVocab = isVocabInvalid(typedV)
			case "@language":
				defLang = typedV
			default:
				aliases[field] = typedV
			}
		default:
			// Error
			invalid = true
			return invalid, false, false, ""
		}
	}
	return invalid, noVocab, impVocab, defLang
}

func validateContextList(lst []interface{}, aliases map[string]string) (invalidContext bool, noVocabulary bool, impVocabulary bool, defLang string) {
	invalidContext = false
	noVocabulary = true
	impVocabulary = true
	for _, item := range lst {
		switch typedV := item.(type) {
		case string:
			// check vocab
			noVocabulary = false
			tempInvalid := isVocabInvalid(typedV)
			if tempInvalid == false {
				// If we find a single valid it can work
				impVocabulary = false
			}
		case map[string]interface{}:
			// check object
			invC, noV, impV, lang := validateContextObject(typedV, aliases)
			if invC == true {
				// Broken object, context invalid
				invalidContext = true
				return true, false, false, ""
			}
			if noV == false {
				// We found a vocab...
				noVocabulary = false
				if impV == false {
					// ...and it is valid
					impVocabulary = false
				}
			}
			if defLang == "" {
				defLang = lang
			}
		default:
			// Invalid item
			return true, false, false, ""
		}
	}
	return invalidContext, noVocabulary, impVocabulary, defLang
}
