package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"testing"
	//"reflect"
	//"encoding/json"
)

func TestDecodeJSON(t *testing.T) {
	testData := `{"foo":"bar", "baz":["quux", {"x":100}]}`
	obj, err := DecodeJSON([]byte(testData))
	if err != nil {
		t.Fatal("DecodeJSON failure:", err)
	}
	if (len(obj.Data) != 2) || (obj.Data["foo"].(string) != "bar") ||
		(len(obj.Data["baz"].([]interface{})) != 2) ||
		(obj.Data["baz"].([]interface{})[0].(string) != "quux") ||
		(obj.Data["baz"].([]interface{})[1].(map[string]interface{})["x"].(float64) != 100.0) {
		t.Fatal("DecodeJSON failure:", obj.Data)
	}
}
