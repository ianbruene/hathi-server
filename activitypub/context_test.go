package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"reflect"
	"testing"
)

func TestBuiltinType(t *testing.T) {
	// Test non-existent type
	typeD := BuiltinType("jabber jabber jabber")
	if typeD != nil {
		t.Fatal("BuiltinType failure:", typeD)
	}
	// Test existing type
	mockNode := TypeNode{false, []string{"iri"}}
	typeD = BuiltinType("@id")
	if reflect.DeepEqual(mockNode, *typeD) == false {
		t.Fatal("BuiltinType failure:", typeD)
	}
}

func Test_validateString(t *testing.T) {
	// Test invalid
	if validateString(42) == true {
		t.Fatal("validateString failure: 42")
	}
	// Test valid
	if validateString("foo") == false {
		t.Fatal("validateString failure: foo")
	}
}

func Test_validateFloat(t *testing.T) {
	// Test invalid
	if validateFloat(42) == true {
		t.Fatal("validateFloat failure: 42")
	}
	// Test valid
	if validateFloat(1.25) == false {
		t.Fatal("validateFloat failure: 1.25")
	}
}

func Test_validateBool(t *testing.T) {
	// Test invalid
	if validateBool(42) == true {
		t.Fatal("validateBool failure: 42")
	}
	// Test valid
	if validateBool(false) == false {
		t.Fatal("validateBool failure: false")
	}
}

func Test_validateComplexData(t *testing.T) {
	// Test invalid
	if validateComplexData(42) == true {
		t.Fatal("validateComplexData 42 failure")
	}
	// Test not a complex data
	if validateComplexData(map[string]interface{}{"@type": "foo"}) == true {
		t.Fatal("validateComplexData @type failure")
	}
	// Test not a complex data, alt type
	if validateComplexData(map[string]interface{}{"type": "foo"}) == true {
		t.Fatal("validateComplexData type failure")
	}
	// Test valid
	if validateComplexData(map[string]interface{}{"foo": 42}) == false {
		t.Fatal("validateComplexData valid failure")
	}
}

func Test_validateIRI(t *testing.T) {
	// Test invalid
	if validateIRI("argle-blarghle") == true {
		t.Fatal("validateIRI invalid failure")
	}
	// Test valid
	if validateIRI("http://foo.test/bleh#meh") == false {
		t.Fatal("validateIRI valid failure")
	}
}

func Test_validateSubObjet(t *testing.T) {
	// Test invalid
	if validateSubObject(42) == true {
		t.Fatal("validateComplexData 42 failure")
	}
	// Test invalid, complex data
	if validateSubObject(map[string]interface{}{"foo": 42}) == true {
		t.Fatal("validateComplexData valid failure")
	}
	// Test valid
	if validateSubObject(map[string]interface{}{"@type": "foo"}) == false {
		t.Fatal("validateComplexData @type failure")
	}
	// Test valid, alt type
	if validateSubObject(map[string]interface{}{"type": "foo"}) == false {
		t.Fatal("validateComplexData type failure")
	}
}

func Test_validateIRIObject(t *testing.T) {
	// Test object
	if validateIRIObject(map[string]interface{}{"@type": "foo"}) == false {
		t.Fatal("validateIRIObject object failure")
	}
	// Test IRI
	if validateIRIObject("https://foo.test/bar") == false {
		t.Fatal("validateIRIObject IRI failure")
	}
}

func Test_validateDatetime(t *testing.T) {
	// Test invalid
	if validateDatetime("therdiglib") == true {
		t.Fatal("validateDatetime invalid failure")
	}
	// Test valid
	if validateDatetime("2006-01-02T15:04:05.999Z") == false {
		t.Fatal("validateDatetime valid failure")
	}
}

func TestClipIRI(t *testing.T) {
	// Test no clipping
	clipped := ClipIRI("foobar")
	if clipped != "foobar" {
		t.Fatal("ClipIRI failure:", clipped)
	}
	// Test clipping
	clipped = ClipIRI("http://foo.test/bar")
	if clipped != "foo.test/bar" {
		t.Fatal("ClipIRI failure:", clipped)
	}
}

func Test_validateDuration(t *testing.T) {
	// Test invalid
	if validateDuration("qwerty") == true {
		t.Fatal("validateDuration invlaid failure")
	}
	// Test valid
	if validateDuration("P1Y2M3DT5H20M30.123S") == false {
		t.Fatal("validateDuration valid failure")
	}
}
