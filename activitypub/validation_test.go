package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"reflect"
	"testing"
)

func TestFieldType(t *testing.T) {
	// Test term
	iri, compact, term := FieldType("foo")
	if iri != false || compact != false || term != true {
		t.Fatal("Incorrect FieldType:", iri, compact, term)
	}
	// Test IRI
	iri, compact, term = FieldType("http://foo.bar")
	if iri != true || compact != false || term != false {
		t.Fatal("Incorrect FieldType:", iri, compact, term)
	}
	// Test compact
	iri, compact, term = FieldType("alias:field")
	if iri != false || compact != true || term != false {
		t.Fatal("Incorrect FieldType:", iri, compact, term)
	}
}

func TestExpandTerm(t *testing.T) {
	// Test exists
	imp, iri := ExpandTerm("image")
	if imp != true || iri != "www.w3.org/ns/activitystreams#image" {
		t.Fatal("ExpandTerm failure:", imp, iri)
	}
	// Test does not exist
	imp, iri = ExpandTerm("argleblarghl")
	if imp != false || iri != "" {
		t.Fatal("ExpandTerm failure:", imp, iri)
	}
}

func TestExpandCompact(t *testing.T) {
	aliases := map[string]string{"foo": "foo.test/foo"}
	// Test malformed
	imp, iri, tds := ExpandCompact("a:s:d:f", aliases)
	if imp != false && iri != "" && tds != nil {
		t.Fatal("ExpandCompact failure:", imp, iri, tds)
	}
	// Test "as"
	targetNode := TypeNode{true, []string{"iri/obj"}}
	imp, iri, tds = ExpandCompact("as:image", aliases)
	if imp != true || iri != "www.w3.org/ns/activitystreams#image" ||
		reflect.DeepEqual(tds, &targetNode) != true {
		t.Fatal("ExpandCompact failure:", imp, iri, tds)
	}
	// Test undefined
	imp, iri, tds = ExpandCompact("bar:baz", aliases)
	if imp != false || iri != "" || tds != nil {
		t.Fatal("ExpandCompact failure:", imp, iri, tds)
	}
	// Test unimplemented
	imp, iri, tds = ExpandCompact("foo:baz", aliases)
	if imp != false || iri != "foo.test/foo#baz" || tds != nil {
		t.Fatal("ExpandCompact failure:", imp, iri, tds)
	}
	// Test implemented
	// insert temp value
	mockNode := TypeNode{false, []string{"quux"}}
	ExtensionFieldTypes["foo.test/foo#baz"] = mockNode
	imp, iri, tds = ExpandCompact("foo:baz", aliases)
	// remove temp
	delete(ExtensionFieldTypes, "foo.test/foo#baz")
	if imp != true || iri != "foo.test/foo#baz" ||
		reflect.DeepEqual(mockNode, *tds) != true {
		t.Fatal("ExpandCompact failure:", imp, iri, tds)
	}
}

func Test_validateValueSingle(t *testing.T) {
	mockType := TypeNode{false, []string{"float", "string"}}
	// Test no valids
	if validateValueSingle(true, &mockType) == true {
		t.Fatal("validateValueSingle failure: bool accepted")
	}
	// Test valid match
	if validateValueSingle("foo", &mockType) == false {
		t.Fatal("validateValueSingle failure: string not accepted")
	}
}

func TestValidateValue(t *testing.T) {
	// Test single
	mockType := TypeNode{false, []string{"float", "string"}}
	if ValidateValue("foo", &mockType) != true {
		t.Fatal("ValidateValue failure: mishandling single object")
	}
	// Test list where not allowed
	if ValidateValue([]string{"foo", "bar"}, &mockType) != false {
		t.Fatal("ValidateValue failure: did not reject forbidden list")
	}
	// Test list with improper item
	mockType = TypeNode{true, []string{"float", "string"}}
	if ValidateValue([]interface{}{"foo", true, 42.23}, &mockType) != false {
		t.Fatal("ValidateValue failure: didn't reject list with improper value")
	}
	// Test perfect list
	if ValidateValue([]interface{}{"foo", "blue", 42.23}, &mockType) != true {
		t.Fatal("ValidateValue failure: did not accept perfect list")
	}
}

func TestValidateFieldName(t *testing.T) {
	aliases := map[string]string{
		"foo": "foo.test/",
	}
	// Test term implemented
	mockType := TypeNode{true, []string{"iri/obj"}}
	imp, valid, IRI, typeD := ValidateFieldName("icon", aliases)
	if imp != true || valid != true ||
		IRI != "www.w3.org/ns/activitystreams#icon" ||
		reflect.DeepEqual(&mockType, typeD) == false {
		t.Fatal("ValidateFieldName failure:", imp, valid, IRI, typeD)
	}
	// Test term not implemented
	imp, valid, IRI, typeD = ValidateFieldName("quux", aliases)
	if imp != false || valid != false || IRI != "" || typeD != nil {
		t.Fatal("ValidateFieldName failure:", imp, valid, IRI, typeD)
	}
	// Test compact
	imp, valid, IRI, typeD = ValidateFieldName("foo:bar", aliases)
	if imp != false || valid != true || IRI != "foo.test/#bar" ||
		typeD != nil {
		t.Fatal("ValidateFieldName failure:", imp, valid, IRI, typeD)
	}
	// Test IRI
	imp, valid, IRI, typeD = ValidateFieldName("https://quux.test/a", aliases)
	if imp != false || valid != true || IRI != "quux.test/a" ||
		typeD != nil {
		t.Fatal("ValidateFieldName failure:", imp, valid, IRI, typeD)
	}
}

func Test_isVocabInvalid(t *testing.T) {
	inv := isVocabInvalid("foo")
	if inv != true {
		t.Fatal("isVocabInvalid failure: foo")
	}
	inv = isVocabInvalid("https://www.w3.org/ns/activitystreams")
	if inv != false {
		t.Fatal("isVocabInvalid failure: https://www.w3.org/ns/activitystreams")
	}
	inv = isVocabInvalid("http://www.w3.org/ns/activitystreams")
	if inv != false {
		t.Fatal("isVocabInvalid failure: http://www.w3.org/ns/activitystreams")
	}
}

func Test_validateContextObject(t *testing.T) {
	// Test perfect
	testObj := map[string]interface{}{"foo": "foobarbaz", "@language": "en",
		"@vocab": "https://www.w3.org/ns/activitystreams"}
	aliases := map[string]string{}
	aliasTarget := map[string]string{"foo": "foobarbaz"}
	inv, noV, impV, lang := validateContextObject(testObj, aliases)
	if inv == true || noV == true || impV == true || lang != "en" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextObject failure:", inv, noV, impV, lang, aliases)
	}
	// Test wrong vocab
	testObj = map[string]interface{}{"foo": "foobarbaz", "@language": "en",
		"@vocab": "blah"}
	aliases = map[string]string{}
	aliasTarget = map[string]string{"foo": "foobarbaz"}
	inv, noV, impV, lang = validateContextObject(testObj, aliases)
	if inv == true || noV == true || impV == false || lang != "en" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextObject failure:", inv, noV, impV, lang, aliases)
	}
	// Test no vocab
	testObj = map[string]interface{}{"foo": "foobarbaz", "@language": "en"}
	aliases = map[string]string{}
	aliasTarget = map[string]string{"foo": "foobarbaz"}
	inv, noV, impV, lang = validateContextObject(testObj, aliases)
	if inv == true || noV == false || impV == true || lang != "en" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextObject failure:", inv, noV, impV, lang, aliases)
	}
	// Test bad type
	testObj = map[string]interface{}{"bar": 42}
	aliases = map[string]string{}
	aliasTarget = map[string]string{}
	inv, noV, impV, lang = validateContextObject(testObj, aliases)
	if inv == false || noV == true || impV == true || lang != "" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextObject failure:", inv, noV, impV, lang, aliases)
	}
}

func Test_validateContextList(t *testing.T) {
	// Test perfect
	testList := []interface{}{"https://www.w3.org/ns/activitystreams",
		map[string]interface{}{"foo": "foobarbaz"}}
	aliases := map[string]string{}
	aliasTarget := map[string]string{"foo": "foobarbaz"}
	inV, noV, impV, lang := validateContextList(testList, aliases)
	if inV != false || noV != false || impV != false || lang != "" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextList failure:", inV, noV, impV, lang, aliases)
	}
	// Test invalid list item
	testList = []interface{}{42, "https://www.w3.org/ns/activitystreams",
		map[string]interface{}{"foo": "foobarbaz"}}
	aliases = map[string]string{}
	aliasTarget = map[string]string{}
	inV, noV, impV, lang = validateContextList(testList, aliases)
	if inV != true || noV != false || impV != false || lang != "" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextList failure:", inV, noV, impV, lang, aliases)
	}
	// Test broken object
	testList = []interface{}{"https://www.w3.org/ns/activitystreams",
		map[string]interface{}{"foo": 42}}
	aliases = map[string]string{}
	aliasTarget = map[string]string{}
	inV, noV, impV, lang = validateContextList(testList, aliases)
	if inV != true || noV != false || impV != false || lang != "" ||
		reflect.DeepEqual(aliases, aliasTarget) == false {
		t.Fatal("validateContextList failure:", inV, noV, impV, lang, aliases)
	}
}

func TestValidateContext(t *testing.T) {
	var data interface{}
	// Test string
	report := new(ValidationReport)
	data = "https://www.w3.org/ns/activitystreams"
	aliases := ValidateContext(data, report)
	if report.InvalidContext != false || report.ImproperVocab != false ||
		reflect.DeepEqual(aliases, map[string]string{}) != true {
		t.Fatal("ValidateContext string failure:", report.InvalidContext,
			report.ImproperVocab, aliases)
	}
	// Test object
	report = new(ValidationReport)
	data = map[string]interface{}{"foo": "foobarbaz", "@language": "en",
		"@vocab": "https://www.w3.org/ns/activitystreams"}
	aliases = ValidateContext(data, report)
	if report.InvalidContext != false || report.ImproperVocab != false ||
		report.MissingVocab != false || report.DefaultLanguage != "en" ||
		reflect.DeepEqual(aliases, map[string]string{"foo": "foobarbaz"}) != true {
		t.Fatal("ValidateContext object failure:", report.InvalidContext,
			report.ImproperVocab, aliases)
	}
	// Test list
	report = new(ValidationReport)
	data = []interface{}{"https://www.w3.org/ns/activitystreams"}
	aliases = ValidateContext(data, report)
	if report.InvalidContext != false || report.ImproperVocab != false ||
		report.MissingVocab != false ||
		reflect.DeepEqual(aliases, map[string]string{}) != true {
		t.Fatal("ValidateContext list failure:", report.InvalidContext,
			report.ImproperVocab, aliases)
	}
	// Test invalid
	report = new(ValidationReport)
	data = 42
	aliases = ValidateContext(data, report)
	if report.InvalidContext != true || aliases != nil {
		t.Fatal("ValidateContext invalid failure:", report.InvalidContext,
			aliases)
	}
}

func TestValidateField(t *testing.T) {
	aliases := map[string]string{"foo": "https://foo.test/"}
	// Test @id
	valid, implemented, validType := ValidateField("@id", "http://quux.test",
		aliases)
	if valid != true || implemented != true || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test id
	valid, implemented, validType = ValidateField("id", "http://quux.test",
		aliases)
	if valid != true || implemented != true || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test @type
	valid, implemented, validType = ValidateField("@type", "http://quux.test",
		aliases)
	if valid != true || implemented != true || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test type
	valid, implemented, validType = ValidateField("type", "http://quux.test",
		aliases)
	if valid != true || implemented != true || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test non-special implemented field
	valid, implemented, validType = ValidateField("icon", "http://quux.test",
		aliases)
	if valid != true || implemented != true || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test non-implemented field
	valid, implemented, validType = ValidateField("https://foo.test/#quux",
		"http://quux.test", aliases)
	if valid != true || implemented != false || validType != true {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
	// Test incorrect type
	valid, implemented, validType = ValidateField("icon", 42, aliases)
	if valid != true || implemented != true || validType != false {
		t.Fatal("ValidateField failure:", valid, implemented, validType)
	}
}

func Test_map2objConversionLoop(t *testing.T) {
	sourceData := map[string]interface{}{"@context": "foo",
		"@id": "http://bar.test/", "@type": "Accept",
		"actor": "https://foo.test/joe", "test:fiddlesticks": 42,
		"test:dream": map[string]interface{}{"test:x": 23, "test:y": 64,
			"test:z": 128}}
	resultData := map[string]interface{}{}
	targetData := map[string]interface{}{"@context": "foo",
		"@id": "http://bar.test/", "@type": "Accept",
		"actor": "https://foo.test/joe", "test:fiddlesticks": 42,
		"test:dream": map[string]interface{}{"test:x": 23, "test:y": 64,
			"test:z": 128}}
	aliases := map[string]string{"test": "https://foo.test/"}
	report := new(ValidationReport)
	map2objConversionLoop(resultData, sourceData, aliases, report)
	if len(report.InvalidFieldNames) != 0 ||
		len(report.UnknownFieldNames) != 5 ||
		len(report.BadFieldTypes) != 0 ||
		reflect.DeepEqual(resultData, targetData) != true {
		t.Fatal("map2objConversionLoop failure:", report.InvalidFieldNames,
			report.UnknownFieldNames, report.BadFieldTypes, resultData)
	}
}

func TestValidateType(t *testing.T) {
	aliases := map[string]string{"foo": "http://foo.test"}
	// Test single term, exists
	if ValidateType("Accept", aliases) != true {
		t.Fatal("ValidateType single-term-existing failure")
	}
	// Test single term, doesn't exist
	if ValidateType("Therdiglib", aliases) != false {
		t.Fatal("ValidateType single-term-non-existing failure")
	}
	// Test single alias, exists
	if ValidateType("foo:bar", aliases) != true {
		t.Fatal("ValidateType single-alias-existing failure")
	}
	// Test single alias, doesn't exist
	if ValidateType("quux:bar", aliases) != false {
		t.Fatal("ValidateType single-alias-existing failure")
	}
	// Test single IRI, valid
	if ValidateType("http://baz.test/", aliases) != true {
		t.Fatal("ValidateType valid IRI failure")
	}
	// Test single IRI, invalid
	if ValidateType("://baa$ff532,fest/", aliases) != false {
		t.Fatal("ValidateType invalid IRI failure")
	}
	// Test multi valid
	valid := ValidateType([]interface{}{
		"Accept", "foo:bar", "http://baz.test/"},
		aliases)
	if valid != true {
		t.Fatal("ValidateType multi-valid failure")
	}
	// Test multi invalid
	valid = ValidateType([]interface{}{
		"Accept", "foo:bar", "http://baz.test/", 42},
		aliases)
	if valid != false {
		t.Fatal("ValidateType multi-valid failure")
	}
}
