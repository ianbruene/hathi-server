*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# Hathi Server

## Developing

**Dependencies**

duration
go-sqlite3
go-sqlmock.v1
hathi-protocol

The build scripts use golang's ability to fetch dependencies as needed.

**1. Install the Go language**
Follow the official instructions at [GoLang](https://golang.org/doc/install) or
```
sudo apt-get install golang
```

Hathi requires golang version 1.11 minimum, earlier versions of Ubuntu can use
this backport PPA:

sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt-get update
sudo apt-get install golang-go

**2. Clone from repository**
```
git clone gitlab.com/hathi-social/hathi-server
```

**3. Build server**
```
make build
```

The repo should *not* be within GOPATH. This project makes use of Go's new
module system, which does not work properly when placed under GOPATH.

**4. Run Server**
```
./server -port <port> -host <host>
```
Port and Host are optional, if ommited the server will default to running on
localhost:8080

## Testing

```
make test
```

## Dumping / Restoring the database to a file

hathi-server has the -dump <file> option, which exports the database in JSON
format to the specified file and exits. Paired with the -restore <file> option
allows the server admin to update their version of hathi-server without
potential database incompatabilities.

NOTE: This system has not been updated yet after some major refactors, and
probably does not work as a result.
